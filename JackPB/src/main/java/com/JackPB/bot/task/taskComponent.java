package com.JackPB.bot.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.JackPB.bot.service.ArbitrageService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class taskComponent {
	
	@Autowired ArbitrageService arbitrageService;
	
	@Scheduled(fixedDelay=5000)
	public void Task () {
		log.info("Task");
		this.arbitrageService.botInit();

}
}
