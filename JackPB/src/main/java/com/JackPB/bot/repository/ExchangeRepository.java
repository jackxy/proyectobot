package com.JackPB.bot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.JackPB.bot.entity.ActiveExchange;


@Repository
public interface ExchangeRepository  extends JpaRepository<ActiveExchange, Integer>{
	List<ActiveExchange> findByActiveTrue();

}
