package com.JackPB.bot.exchange;

import java.math.BigDecimal;

import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.kucoin.KucoinExchange;




public class KucoinSpecs extends ExchangeSpecs {

	private String passphrase;

	public KucoinSpecs(String apiKey, String secretKey, String exchangeName, BigDecimal buyfee, BigDecimal sellFee,
			String passphrase) {
		super(apiKey, secretKey, "Jackxy@me.com", exchangeName, buyfee, sellFee);
		this.passphrase = passphrase;

		if (null != apiKey && null != secretKey) {
			setRealMode(true);
		}
	}

	public KucoinSpecs() {
		super();

	}

	@Override
	public ExchangeSpecification getSetupedExchange() {
		ExchangeSpecification exSpec = new KucoinExchange().getDefaultExchangeSpecification();
		if (super.getRealMode()) {
			exSpec.setUserName(super.getUserName());
			exSpec.setApiKey(super.getApiKey());
			exSpec.setSecretKey(super.getSecretKey());

		}
		return exSpec;

	}

	public String getPassphrase() {
		return passphrase;
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

}
