package com.JackPB.bot.exchange;

import org.knowm.xchange.Exchange;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivatedExchange {
	
	private Exchange exchange;
	private Boolean activated;
	private Boolean realMode;

}
