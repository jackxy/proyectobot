package com.JackPB.bot.exchange;

import java.math.BigDecimal;

import org.knowm.xchange.ExchangeSpecification;

public abstract class ExchangeSpecs {
	private String userName;
	private String exchangeName;
	private String apiKey;
	private String secretKey;
	private BigDecimal buyFee;
	private BigDecimal sellFee;
	private Boolean realMode = false;

	protected ExchangeSpecs(String apiKey, String secretKey, String userName, String exchangeName, BigDecimal buyFee,
			BigDecimal sellFee) {
		this.userName = userName;
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.buyFee = buyFee;
		this.sellFee = sellFee;
		this.exchangeName = exchangeName;
		

	}

	protected ExchangeSpecs() {

	}

	protected String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}

	protected String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	protected String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public BigDecimal getBuyFee() {
		return buyFee;
	}

	public void setBuyFee(BigDecimal buyFee) {
		this.buyFee = buyFee;
	}

	public BigDecimal getSellFee() {
		return sellFee;
	}

	public void setSellFee(BigDecimal sellFee) {
		this.sellFee = sellFee;
	}

	public Boolean getRealMode() {
		return realMode;
	}

	public void setRealMode(Boolean realMode) {
		this.realMode = realMode;
	}

	public ExchangeSpecification getSetupedExchange() {
		return null;

	}

}
