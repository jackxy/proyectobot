package com.JackPB.bot.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.JackPB.bot.configuration.SelectedExchange;
import com.JackPB.bot.entity.ActiveExchange;
import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.exchange.ExchangeSpecs;
import com.JackPB.bot.service.ArbitrageService;
import com.JackPB.bot.service.ExchangeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ArbitrageServiceimpl implements ArbitrageService {

	@Autowired
	ExchangeService exchangeService;

	@Override

	public void botInit() {
		log.info("BOT INIT");
		List<ActiveExchange> activeExchanges = this.exchangeService.findByActiveExchange();
		ArrayList<ExchangeSpecs> selectedExchanges = SelectedExchange.getSelectedExchange(activeExchanges)
				.getExchange();
		ArrayList<ActivatedExchange>activatedExchanges = this.exchangeService.getActivatedExchanges(selectedExchanges);

	}
}
