package com.JackPB.bot.service;

import java.util.ArrayList;
import java.util.List;

import com.JackPB.bot.entity.ActiveExchange;
import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.exchange.ExchangeSpecs;

public interface ExchangeService {

	List<ActiveExchange> findByActiveExchange();

	ArrayList<ActivatedExchange> getActivatedExchanges(ArrayList<ExchangeSpecs> selectedExchanges);

}
