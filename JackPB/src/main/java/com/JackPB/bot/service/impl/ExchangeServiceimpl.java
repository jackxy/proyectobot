package com.JackPB.bot.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.JackPB.bot.entity.ActiveExchange;
import com.JackPB.bot.exchange.ActivatedExchange;
import com.JackPB.bot.exchange.ExchangeSpecs;
import com.JackPB.bot.repository.ExchangeRepository;
import com.JackPB.bot.service.ExchangeService;

@Service
public class ExchangeServiceimpl implements ExchangeService {

	@Autowired
	ExchangeRepository exchangeRepository;

	@Override

	public List<ActiveExchange> findByActiveExchange() {
		return this.exchangeRepository.findByActiveTrue();
	}

	public ArrayList<ActivatedExchange> getActivatedExchanges(ArrayList<ExchangeSpecs> selectedExchanges) {
		return selectedExchanges.parallelStream().map(selecExchange -> this.getActiveExchange(selecExchange))
				.collect(Collectors.toCollection(ArrayList::new));

	}

	private ActivatedExchange getActiveExchange(ExchangeSpecs exchangeSpecs) {
		Exchange exchange = ExchangeFactory.INSTANCE.createExchange(exchangeSpecs.getSetupedExchange());
		return ActivatedExchange.builder().activated(true).exchange(exchange).realMode(false).build();
	}

}
